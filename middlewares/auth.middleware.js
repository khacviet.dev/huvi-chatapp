require('dotenv').config();
const jwt = require('jsonwebtoken');

const HttpError = require('../models/http-error');

module.exports = (req, res, next) => {
	if (req.method === 'OPTIONS') {
		return next();
	}
	try {
		const token = req.cookies['access_token'];
		const cUser = req.cookies['c_user'];

		if (!cUser || !token) {
			res.clearCookie('access_token');
			res.clearCookie('c_user');

			const error = new HttpError('Authentication failed!', 403);
			return next(error);
		}

		jwt.verify(token, process.env.ACCESS_TOKEN_SERCRET_KEY, (err, decoded) => {
			if (err || cUser !== decoded.userId) {
				res.clearCookie('access_token');
				res.clearCookie('c_user');

				const error = new HttpError('Authentication failed!', 403);
				return next(error);
			}
			req.userData = { userId: decoded.userId };
			next();
		});
	} catch (err) {
		res.clearCookie('access_token');
		res.clearCookie('c_user');

		const error = new HttpError('Authentication failed!', 403);
		return next(error);
	}
};
