require('dotenv').config();

const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');
const sockets = require('./config/socket.config');
require('./config/passport.config');

const authMiddleware = require('./middlewares/auth.middleware');

const authRoutes = require('./routes/auth.route');
const userRoutes = require('./routes/user.route');
const channelRoutes = require('./routes/channel.route');
const HttpError = require('./models/http-error');

const app = express();
const server = require('http').Server(app);

app.use(
	helmet({
		contentSecurityPolicy: false,
	})
);

const whitelist = ['http://localhost:3000', 'http://localhost:4000'];

const corsProd = {
	origin: '*',
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
	allowedHeaders: 'Authorization,Origin,X-Requested-With,Content-Type,Accept',
	credentials: true,
};

const corsDev = {
	origin: (origin, callback) => {
		if (whitelist.indexOf(origin) !== -1) {
			callback(null, true);
		} else {
			callback(new Error(`Cors policy`));
		}
	},
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
	allowedHeaders: 'Authorization,Origin,X-Requested-With,Content-Type,Accept',
	credentials: true,
};

app.use(express.urlencoded({ extended: true, limit: '25mb' }));
app.use(express.json({ limit: '1mb' }));
app.use(cookieParser());

app.use('/auth', cors(corsProd), authRoutes);
app.use(
	'/user',
	process.env.ENVIRONMENT === 'prod' ? cors(corsProd) : cors(corsDev),
	authMiddleware,
	userRoutes
);
app.use(
	'/channel',
	process.env.ENVIRONMENT === 'prod' ? cors(corsProd) : cors(corsDev),
	authMiddleware,
	channelRoutes
);

if (process.env.ENVIRONMENT === 'prod') {
	app.use(express.static(path.join(__dirname, 'client')));
	app.get('*', (req, res) => {
		res.sendFile(path.join(__dirname + '/client/index.html'));
	});
}

app.use(() => {
	const error = new HttpError('Could not find this route.', 404);
	throw error;
});

app.use((error, req, res, next) => {
	if (res.headersSent) {
		return next();
	}
	res.status(error.code || 500);
	res.json({ message: error.message || 'An unknown error occurred!' });
});

server.listen(process.env.PORT || 4000, () => {
	console.log('Listen on port 4000');
	sockets.init(server);
});
