require('dotenv').config();

const cookie = require('cookie');
const jwt = require('jsonwebtoken');
const HttpError = require('../models/http-error');
const messageControllers = require('../controllers/message.controller');
const { client } = require('./redis.config');
const { v4: uuidv4 } = require('uuid');
const pool = require('./db.config');

let sockets = {};

const whitelist = ['http://localhost:3000', 'http://localhost:4000'];

sockets.init = (server) => {
	// init server with cors policy
	const io = require('socket.io')(server, {
		cors: {
			origin:
				process.env.ENVIRONMENT === 'prod'
					? '*'
					: (origin, callback) => {
							if (whitelist.indexOf(origin) !== -1) {
								callback(null, true);
							} else {
								callback(new Error('Cors policys.'));
							}
					  },
			methods: ['GET', 'POST'],
			allowedHeaders: [
				'Authorization,Origin,X-Requested-With,Content-Type,Accept',
			],
			credentials: true,
		},
	});

	io.use(async (socket, next) => {
		// User verification by cookie
		if (!socket.request.headers.cookie) {
			const error = new HttpError('Authentication failed!', 403);
			return next(error);
		}

		const cookieObject = cookie.parse(socket.request.headers.cookie);
		const token = cookieObject['access_token'];

		let decodedToken;
		try {
			decodedToken = jwt.verify(token, process.env.ACCESS_TOKEN_SERCRET_KEY);
		} catch (err) {
			const error = new HttpError('Authentication failed!', 403);
			return next(error);
		}

		const userId = decodedToken.userId;
		let user;
		try {
			const userQuery = 'SELECT user_id FROM users WHERE user_id = $1';
			const userParams = [userId];
			user = await pool.query(userQuery, userParams);
		} catch (err) {
			const error = new HttpError(
				'Something went wrong, please try again.',
				500
			);
			return next(error);
		}

		if (user.rowCount < 1) {
			const error = new HttpError(
				'Could not find user for the provided id.',
				404
			);
			return next(error);
		}

		const updateUserQuery = `UPDATE users
        SET socket_id=$1, is_online=$2
        WHERE user_id = $3`;
		const updateUserParams = [socket.id, true, userId];

		try {
			await pool.query(updateUserQuery, updateUserParams);
		} catch (err) {
			const error = new HttpError(
				'Something went wrong, please try again',
				500
			);
			return next(error);
		}

		next();
	}).on('connection', async (socket) => {
		const cookieObject = cookie.parse(socket.request.headers.cookie);
		const userId = cookieObject['c_user'];

		const channelUserBelongQuery = `SELECT channel_id FROM channel_user WHERE user_id = $1`;
		const channelUserBelongParams = [userId];

		const channelUserBelong = await pool.query(
			channelUserBelongQuery,
			channelUserBelongParams
		);

		if (channelUserBelong.rowCount > 0) {
			for (let i = 0; i < channelUserBelong.rowCount; i++) {
				socket.join(channelUserBelong.rows[i].channel_id);
			}
		}

		socket.on('requestChannel', async (channelId) => {
			const query = `SELECT socket_id, is_online FROM channel INNER JOIN users ON channel.created_by = users.user_id
			WHERE channel_id = $1`;
			const params = [channelId];

			const host = await pool.query(query, params);

			if (host.rows[0].socket_id !== '' && host.rows[0].is_online === true) {
				io.to(host.rows[0].socket_id).emit('requestChannelResponse', true);
			}
		});

		socket.on('unRequestChannel', async (channelId) => {
			const query = `SELECT socket_id, is_online FROM channel INNER JOIN users ON channel.created_by = users.user_id
			WHERE channel_id = $1`;
			const params = [channelId];

			const host = await pool.query(query, params);

			if (host.rows[0].socket_id !== '' && host.rows[0].is_online === true) {
				io.to(host.rows[0].socket_id).emit('unRequestChannelResponse', false);
			}
		});

		socket.on('approveRequest', async (userRequest) => {
			const query = `SELECT socket_id, is_online FROM users WHERE user_id = $1`;
			const params = [userRequest];

			const user = await pool.query(query, params);

			if (user.rows[0].socket_id !== '' && user.rows[0].is_online === true) {
				io.to(user.rows[0].socket_id).emit('approveRequestResponse', true);
			}
		});

		// get message in first times message box show
		socket.on('getMessagesFirst', async (msg) => {
			client.exists(userId, (err, exist) => {
				if (exist === 1) {
					client.get(userId, (err, result) => {
						const parseValue = JSON.parse(result);
						parseValue[msg.channelId] = false;

						client.set(userId, JSON.stringify(parseValue));
					});
				}
			});

			const messages = await messageControllers.getMessageByChannel(
				msg.channelId,
				0,
				20
			);

			socket.emit('haveMessage', false);

			socket.emit('getMessagesFirstResponse', messages);
		});

		// get messages when user scroll up
		socket.on('getMessages', async (msg) => {
			client.exists(userId, (err, exist) => {
				if (exist === 1) {
					client.get(userId, (err, result) => {
						const parseValue = JSON.parse(result);
						parseValue[msg.channelId] = false;

						client.set(userId, JSON.stringify(parseValue));
					});
				}
			});

			const messages = await messageControllers.getMessageByChannel(
				msg.channelId,
				msg.skip,
				20
			);

			socket.emit('haveMessage', false);

			socket.emit('getMessagesResponse', messages, msg.st);
		});

		socket.on('sendMessage', async (msg) => {
			const insertMessageQuery = `INSERT INTO message(
                message_id, sender_id, channel_id, content, content_type, created_at)
                VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`;
			const insertMessageParams = [
				uuidv4(),
				userId,
				msg.channelId,
				msg.message,
				0,
				new Date(),
			];

			const insertMessage = await pool.query(
				insertMessageQuery,
				insertMessageParams
			);

			const userQuery = `SELECT user_id FROM channel_user WHERE channel_id = $1 AND user_id <> $2`;
			const userParams = [msg.channelId, userId];

			const users = await pool.query(userQuery, userParams);

			if (users.rowCount > 0) {
				for (let i = 0; i < users.rowCount; i++) {
					const key = users.rows[i].user_id.toString();

					client.exists(key, (err, exist) => {
						if (exist === 0) {
							const objectValue = {
								[msg.channelId]: true,
							};
							client.set(key, JSON.stringify(objectValue));
						} else {
							client.get(key, (err, result) => {
								const parseValue = JSON.parse(result);
								parseValue[msg.channelId] = true;

								client.set(key, JSON.stringify(parseValue));
							});
						}
					});
				}
			}

			client.exists(userId, (err, exist) => {
				if (exist === 1) {
					client.get(userId, (err, result) => {
						const parseValue = JSON.parse(result);
						parseValue[msg.channelId] = false;

						client.set(userId, JSON.stringify(parseValue));
					});
				}
			});
			socket.emit('haveMessage', false);
			socket.to(msg.channelId).emit('haveMessage', true);

			insertMessage.rows[0].avt_link = msg.avt;
			insertMessage.rows[0].display_name = msg.displayName;

			io.in(msg.channelId).emit(
				'sendMessageResponse',
				insertMessage.rows[0],
				msg.channelId
			);
		});

		socket.on('getIsRead', () => {
			client.exists(userId, (err, exist) => {
				if (exist === 1) {
					client.get(userId, (err, result) => {
						socket.emit('getIsReadResponse', JSON.parse(result));
					});
				} else {
					socket.emit('getIsReadResponse', {});
				}
			});
		});

		socket.on('disconnect', async () => {
			try {
				const userQuery = 'SELECT user_id FROM users WHERE socket_id = $1';
				const userParams = [socket.id];
				const user = await pool.query(userQuery, userParams);

				if (user.rowCount > 0) {
					const updateQuery = `UPDATE users
                    SET socket_id=$1, is_online=$2
                    WHERE user_id = $3`;
					const updateParams = ['', false, user.rows[0].user_id];
					await pool.query(updateQuery, updateParams);
				}
			} catch (error) {
				socket.emit('error', 'Something went wrong, please try again');
				return;
			}
		});
	});
};

module.exports = sockets;
