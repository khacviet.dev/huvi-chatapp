require('dotenv').config();
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const pool = require('./db.config');

// GoogleStrategy with passport
passport.use(
	new GoogleStrategy(
		{
			clientID: process.env.GOOGLE_KEY,
			clientSecret: process.env.GOOGLE_SECRET,
			callbackURL: process.env.GOOGLE_CALLBACK_URL,
		},
		async (accessToken, refreshToken, profile, done) => {
			try {
				profile.photos[0].value;
				const currentUserQuery = 'SELECT user_id FROM users WHERE user_id = $1';
				const currentUserParams = [profile.id];
				const currentUser = await pool.query(
					currentUserQuery,
					currentUserParams
				);

				if (currentUser.rowCount < 1) {
					const insertUserQuery = `INSERT INTO users(
						user_id, email, display_name, socket_id, is_online, avt_link)
						VALUES ($1, $2, $3, $4, $5, $6)`;
					const insertUserParams = [
						profile.id,
						profile._json.email,
						profile.displayName,
						'',
						false,
						profile.photos[0].value,
					];
					await pool.query(insertUserQuery, insertUserParams);

					return done(null, profile.id);
				}

				return done(null, profile.id);
			} catch (err) {
				return done(err);
			}
		}
	)
);

module.exports = passport;
