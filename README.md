# Huvi Chat

A simple chat application that connects users with each other via channels

## Tech Stack

NoedJS, ReactJS, Socket.IO, AWS, Docker

## Demo

Click [Huvi](https://huvi.online) to open Education Center CMS  

## Screenshots

![alt text](./screenshots/img1.png)
![alt text](./screenshots/img2.png)
