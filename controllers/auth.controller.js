require('dotenv').config();
const jwt = require('jsonwebtoken');

const signin = (req, res) => {
	const userId = req.user;

	const accessToken = jwt.sign(
		{ userId },
		process.env.ACCESS_TOKEN_SERCRET_KEY,
		{
			expiresIn: '60d',
		}
	);

	res.cookie('access_token', accessToken, {
		maxAge: 24 * 60 * 60 * 60 * 1000,
		httpOnly: true,
	});

	res.cookie('c_user', userId, {
		maxAge: 24 * 60 * 60 * 60 * 1000,
	});

	res.redirect(process.env.CLIENT_HOME_PAGE_URL);
};

const signout = (req, res) => {
	res.clearCookie('access_token');
	res.clearCookie('c_user');

	return res.status(200).json({
		logout: true,
	});
};

module.exports = {
	signin,
	signout,
};
