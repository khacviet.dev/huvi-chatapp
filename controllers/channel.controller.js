const HttpError = require('../models/http-error');
const pool = require('../config/db.config');
const { v4: uuidv4 } = require('uuid');

const getChannels = async (req, res, next) => {
	const userId = req.userData.userId;
	try {
		const channelsQuery = `SELECT (SELECT COUNT(*) FROM channel_user WHERE channel_user.channel_id = channel.channel_id) as count,
		 channel_id, channel_name, channel_avt, COALESCE((SELECT is_request 
			FROM channel_request WHERE channel_request.channel_id = channel.channel_id
			AND user_request_id = $1),false) as is_request FROM channel ORDER BY channel_name`;
		const channelParams = [userId];
		const channels = await pool.query(channelsQuery, channelParams);

		return res.status(200).json(channels.rows);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const getChannelsByUserId = async (req, res, next) => {
	const userId = req.userData.userId;
	try {
		const channelsQuery = `SELECT channel.channel_id, channel.channel_name, channel_avt FROM channel_user
		INNER JOIN channel ON channel.channel_id = channel_user.channel_id WHERE user_id = $1`;
		const channelsParams = [userId];

		const channels = await pool.query(channelsQuery, channelsParams);

		return res.status(200).json(channels.rows);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const getChannelsNotByUserId = async (req, res, next) => {
	const userId = req.userData.userId;
	try {
		const channelsQuery = `SELECT channel_id, channel_name, channel_avt, COALESCE((SELECT is_request 
			FROM channel_request WHERE channel_request.channel_id = channel.channel_id
			AND user_request_id = $1),false) as is_request 
			FROM channel
			WHERE channel_id NOT IN(SELECT channel_id FROM channel_user WHERE user_id= $2)
			ORDER BY channel_name`;
		const channelsParams = [userId, userId];

		const channels = await pool.query(channelsQuery, channelsParams);

		return res.status(200).json(channels.rows);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const getChannelById = async (req, res, next) => {
	const channelId = req.params.channelId;

	try {
		const channelQuery = `SELECT channel_id, channel_name, channel_avt, user_id, email, display_name FROM channel
		INNER JOIN users ON channel.created_by = users.user_id 
		WHERE channel_id = $1`;
		const channelParams = [channelId];

		const channel = await pool.query(channelQuery, channelParams);

		if (channel.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		return res.status(200).json(channel.rows[0]);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const getRequestsChannel = async (req, res, next) => {
	const userId = req.userData.userId;

	try {
		const query = `SELECT user_request_id, display_name, avt_link, channel.channel_name,
		channel.channel_id FROM channel_request INNER JOIN users
	ON channel_request.user_request_id = users.user_id
	INNER JOIN channel ON channel.channel_id = channel_request.channel_id
	WHERE created_by = $1 AND is_approve = false AND is_request = true`;
		const params = [userId];

		const requests = await pool.query(query, params);

		return res.status(200).json(requests.rows);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const createChannel = async (req, res, next) => {
	const userId = req.userData.userId;
	const { channelName } = req.body;
	const client = await pool.connect();

	try {
		await client.query('BEGIN');

		const insertChannelQuery = `INSERT INTO channel(
			channel_id, channel_name, created_by)
			VALUES ($1, $2, $3) RETURNING channel_id`;
		const insertChannelParams = [uuidv4(), channelName, userId];

		const insertChannel = await client.query(
			insertChannelQuery,
			insertChannelParams
		);

		const insertChannelUserQuery = `INSERT INTO channel_user(
				channel_id, user_id)
				VALUES ($1, $2)`;
		const insertChannelUserParams = [insertChannel.rows[0].channel_id, userId];

		await client.query(insertChannelUserQuery, insertChannelUserParams);

		await client.query('COMMIT');

		return res.status(201).json({
			channelId: insertChannel.rows[0].channel_id,
			message: 'Create channel successfully!',
		});
	} catch (err) {
		await client.query('ROLLBACK');
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	} finally {
		client.release();
	}
};

const updateChannelAvatar = async (req, res, next) => {
	const channelId = req.params.channelId;

	try {
		const checkQuery = 'SELECT channel_id FROM channel WHERE channel_id = $1';
		const checkParams = [channelId];

		const check = await pool.query(checkQuery, checkParams);

		if (check.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		const query = `UPDATE channel
		SET channel_avt=$1
		WHERE channel_id = $2`;
		const params = [req.file.location, channelId];

		await pool.query(query, params);

		return res
			.status(200)
			.json({ message: 'Update channel avatar successfully!' });
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const requestChannel = async (req, res, next) => {
	const userId = req.userData.userId;
	const channelId = req.params.channelId;

	try {
		const checkChannelQuery =
			'SELECT channel_id FROM channel WHERE channel_id = $1';
		const checkChannelParams = [channelId];

		const checkChannel = await pool.query(
			checkChannelQuery,
			checkChannelParams
		);

		if (checkChannel.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		const checkQuery =
			'SELECT * FROM channel_request WHERE user_request_id = $1 AND channel_id = $2';
		const checkParams = [userId, channelId];
		const check = await pool.query(checkQuery, checkParams);

		if (check.rowCount > 0) {
			if (check.rows[0].is_approve === true) {
				const error = new HttpError('User already belongs to the channel', 409);
				return next(error);
			}
			if (check.rows[0].is_request === true) {
				const error = new HttpError(
					'User already request to join the channel',
					409
				);
				return next(error);
			}

			const updateQuery = `UPDATE channel_request
			SET is_request = $1
			WHERE user_request_id = $2 AND channel_id = $3`;
			const updateParams = [true, userId, channelId];

			await pool.query(updateQuery, updateParams);
		} else {
			const insertQuery = `INSERT INTO channel_request(
				user_request_id, channel_id, is_request, is_approve)
				VALUES ($1, $2, $3, $4)`;
			const insertParams = [userId, channelId, true, false];

			await pool.query(insertQuery, insertParams);
		}

		return res.status(201).json({ message: 'Request channel successfully!' });
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const unRequestChannel = async (req, res, next) => {
	const userId = req.userData.userId;
	const channelId = req.params.channelId;

	try {
		const checkChannelQuery =
			'SELECT channel_id FROM channel WHERE channel_id = $1';
		const checkChannelParams = [channelId];

		const checkChannel = await pool.query(
			checkChannelQuery,
			checkChannelParams
		);

		if (checkChannel.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		const checkQuery =
			'SELECT * FROM channel_request WHERE user_request_id = $1 AND channel_id = $2';
		const checkParams = [userId, channelId];
		const check = await pool.query(checkQuery, checkParams);

		if (check.rowCount > 0) {
			if (check.rows[0].is_approve === true) {
				const error = new HttpError('User already belongs to the channel', 409);
				return next(error);
			}
			if (check.rows[0].is_request === false) {
				const error = new HttpError(
					`User haven't request to join the channel yet`,
					409
				);
				return next(error);
			}

			const updateQuery = `UPDATE channel_request
			SET is_request = $1
			WHERE user_request_id = $2 AND channel_id = $3`;
			const updateParams = [false, userId, channelId];

			await pool.query(updateQuery, updateParams);
		}

		return res.status(201).json({ message: 'Unrequest channel successfully!' });
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const approveRequestChannel = async (req, res, next) => {
	const userId = req.userData.userId;

	const channelId = req.params.channelId;

	const { userRequest } = req.body;
	const client = await pool.connect();

	try {
		await client.query('BEGIN');

		const checkChannelQuery =
			'SELECT channel_id, created_by FROM channel WHERE channel_id = $1';
		const checkChannelParams = [channelId];

		const checkChannel = await client.query(
			checkChannelQuery,
			checkChannelParams
		);

		if (checkChannel.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		if (userId !== checkChannel.rows[0].created_by) {
			const error = new HttpError(
				'You do not have permission to approve this channel',
				404
			);
			return next(error);
		}

		const updateQuery = `UPDATE channel_request
			SET is_approve = $1
			WHERE user_request_id = $2 AND channel_id = $3`;
		const updateParams = [true, userRequest, channelId];

		await pool.query(updateQuery, updateParams);

		const insertQuery = `INSERT INTO channel_user(
			channel_id, user_id)
			VALUES ($1, $2)`;
		const insertParams = [channelId, userRequest];

		await pool.query(insertQuery, insertParams);

		await client.query('COMMIT');
		return res.status(201).json({ message: 'Request channel successfully!' });
	} catch (err) {
		await client.query('ROLLBACK');
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	} finally {
		client.release();
	}
};

const leaveChannel = async (req, res, next) => {
	const userId = req.userData.userId;
	const channelId = req.params.channelId;
	const client = await pool.connect();

	try {
		await client.query('BEGIN');

		const checkChannelQuery =
			'SELECT channel_id FROM channel WHERE channel_id = $1';
		const checkChannelParams = [channelId];

		const checkChannel = await client.query(
			checkChannelQuery,
			checkChannelParams
		);

		if (checkChannel.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		const deleteQuery = `DELETE FROM channel_user
		WHERE channel_id = $1 AND user_id = $2`;
		const deleteParams = [channelId, userId];
		await pool.query(deleteQuery, deleteParams);

		const deleteRequestQuery = `DELETE FROM channel_request
		WHERE user_request_id = $1 AND channel_id = $2`;
		const deleteRequestParams = [userId, channelId];
		await pool.query(deleteRequestQuery, deleteRequestParams);

		await client.query('COMMIT');

		return res.status(200).json({ message: 'Leave channel successfully!' });
	} catch (err) {
		await client.query('ROLLBACK');
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	} finally {
		client.release();
	}
};

module.exports = {
	getChannels,
	getChannelsByUserId,
	getChannelsNotByUserId,
	createChannel,
	requestChannel,
	unRequestChannel,
	approveRequestChannel,
	leaveChannel,
	getChannelById,
	getRequestsChannel,
	updateChannelAvatar,
};
