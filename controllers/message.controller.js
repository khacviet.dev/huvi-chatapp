const HttpError = require('../models/http-error');
const pool = require('../config/db.config');

const getMessageByChannel = async (channelId, offset, limit) => {
	try {
		const messagesQuery = `SELECT message_id, sender_id, content, content_type, created_at, display_name, avt_link
        FROM message INNER JOIN users ON message.sender_id = users.user_id
        WHERE channel_id = $1 
        ORDER BY created_at DESC OFFSET $2 LIMIT $3`;
		const messagesParams = [channelId, offset, limit];

		const messages = await pool.query(messagesQuery, messagesParams);

		messages.rows.reverse();
		return messages.rows;
	} catch (err) {
		return [];
	}
};

module.exports = {
	getMessageByChannel,
};
