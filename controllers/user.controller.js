const HttpError = require('../models/http-error');
const pool = require('../config/db.config');

const getMe = async (req, res, next) => {
	const userId = req.userData.userId;
	try {
		const query = 'SELECT * FROM users WHERE user_id = $1';
		const params = [userId];

		const user = await pool.query(query, params);

		return res.status(200).json(user.rows[0]);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

const getUsersByChannel = async (req, res, next) => {
	const channelId = req.params.channelId;

	try {
		const checkChannelQuery =
			'SELECT channel_id FROM channel WHERE channel_id = $1';
		const checkChannelParams = [channelId];

		const checkChannel = await pool.query(
			checkChannelQuery,
			checkChannelParams
		);

		if (checkChannel.rowCount < 1) {
			const error = new HttpError(
				'Could not find channel for the provided id',
				404
			);
			return next(error);
		}

		const query = `SELECT users.user_id, display_name, email, avt_link FROM users INNER JOIN channel_user
			 ON users.user_id = channel_user.user_id WHERE channel_id = $1`;
		const params = [channelId];
		const users = await pool.query(query, params);

		return res.status(200).json(users.rows);
	} catch (err) {
		const error = new HttpError('Something went wrong, please try again.', 500);
		return next(error);
	}
};

module.exports = {
	getMe,
	getUsersByChannel,
};
