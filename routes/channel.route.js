const express = require('express');
const channelControllers = require('../controllers/channel.controller');
const upload = require('../config/upload.config');
const router = express.Router();

router.get('/', channelControllers.getChannels);

router.get('/me', channelControllers.getChannelsByUserId);

router.get('/notme', channelControllers.getChannelsNotByUserId);

router.get('/request', channelControllers.getRequestsChannel);

router.get('/:channelId', channelControllers.getChannelById);

router.post('/create', channelControllers.createChannel);

router.post('/request/:channelId', channelControllers.requestChannel);

router.post('/unrequest/:channelId', channelControllers.unRequestChannel);

router.post('/approve/:channelId', channelControllers.approveRequestChannel);

router.post('/leave/:channelId', channelControllers.leaveChannel);

router.patch(
	'/:channelId',
	upload.single('image'),
	channelControllers.updateChannelAvatar
);

module.exports = router;
