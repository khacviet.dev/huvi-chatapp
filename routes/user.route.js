const express = require('express');
const userControllers = require('../controllers/user.controller');

const router = express.Router();

router.get('/me', userControllers.getMe);
router.get('/:channelId', userControllers.getUsersByChannel);

module.exports = router;
