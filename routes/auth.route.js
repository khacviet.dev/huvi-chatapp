require('dotenv').config();
const router = require('express').Router();
const passport = require('passport');

const authControllers = require('../controllers/auth.controller');

router.get('/login/failed', (req, res) => {
	res.status(401).json({
		success: false,
		message: 'User failed to authenticate',
	});
});

router.get(
	'/google',
	passport.authenticate('google', {
		session: false,
		scope: ['profile', 'email'],
	})
);

router.get(
	'/google/callback',
	passport.authenticate('google', {
		failureRedirect: '/auth/login/failed',
		session: false,
	}),
	authControllers.signin
);

router.post('/signout', authControllers.signout);

module.exports = router;
